<?php


class Conexao{
  private static $PASS="root";
  private static $USER="root";
  private static $DBNAME="CRUDGENEZYS";
  private static $HOST="localhost";
  private static $con;
  public static function getInstance(){
    if (empty(self::$con)){
      try{
        self::$con =new PDO("mysql:dbname=".self::$DBNAME."; host=".self::$HOST, self::$USER, self::$PASS);
        self::$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return self::$con;
      }
      catch (Exception $e){
        echo 'Exceção capturada: ',  $e->getMessage(), "\n";
        return null;
      }
    }
  }
}

?>
