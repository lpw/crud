<?php

require_once 'TarefaDAO.php';

class TarefaController{
  private $tarefadao;

function __construct($dao){
  $this->tarefadao=$dao;
}
  function Get($id){
    if(!empty($id)){
      $tarefa=$this->tarefadao->Find($id);
      echo json_encode($tarefa);
    }
  }

  function ListAll(){
    $tarefas=$this->tarefadao->ListAll();
    echo json_encode($tarefas);
  }

  function Put($id){
    $data = (object) json_decode(file_get_contents('php://input'), true);
    if ($id === $data->id){
      $tarefa=$this->tarefadao->Save($data);
      echo $data->id;
    }
  }
  function Post(){
    $data = (object) json_decode(file_get_contents('php://input'), true);
    $this->tarefadao->Save($data);
    echo $data->description;
  }

  function Delete($id){
    $this->tarefadao->Delete($id);
  }
}
?>
