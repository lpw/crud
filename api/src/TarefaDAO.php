<?php

require_once "Tarefa.php";
require_once "Conexao.php";


class TarefaDAO{
	//Dados de acesso
	private $conn;


	function Save($tarefa){
		$this->conn=Conexao::getInstance();
		$params=array(':owner'=>$tarefa->owner,
		':description'=>$tarefa->description,
		':done'=>$tarefa->done);

		if (empty($tarefa->id)){
			$sql = "INSERT INTO TAREFA (owner,description,done) VALUES (:owner,:description,:done)";
			$q=$this->conn->prepare($sql);
	}	else{
			$sql = "UPDATE TAREFA set owner=:owner,description=:description,done=:done WHERE id=:id";
			$q=$this->conn->prepare($sql);
			$params[':id']=$tarefa->id;
		}
		//	$q = 	$this->ligacao->prepare($sql);

		$q->execute($params);
	}


	function Find($id){
		$this->conn=Conexao::getInstance();
		$sql = "SELECT  * FROM  TAREFA WHERE ID = :id";
		$resultados = $this->conn->prepare($sql);
		$resultados->bindParam(":id", $id, PDO::PARAM_STR);
		$resultados->execute();
		$result=$resultados->fetchAll(PDO::FETCH_CLASS, "Tarefa");
		return $result[0];
	}

	function ListAll(){
		$this->conn=Conexao::getInstance();
		$sql = "SELECT  * FROM  TAREFA";
		$resultados = $this->conn->prepare($sql);
		$resultados->execute();
		return $resultados->fetchAll(PDO::FETCH_CLASS, "Tarefa");
	}

	function Delete($id){
		$this->conn=Conexao::getInstance();
		$sql = "DELETE FROM TAREFA WHERE id=:id";
		$resultados = $this->conn->prepare($sql);
		$resultados->bindParam(":id", $id, PDO::PARAM_STR);
		$resultados->execute();
	}

	//Em caso de pesquisas, via procedures
	//$pesq = "";
	//$sql = "CALL Nome_da_procedure()";

	//Em caso de querys
	/*$pesq = "Nome_do_Campo";
	$sql = "SELECT * FROM $tabela WHERE nome= :nome_param";

	$resultados = $ligacao->prepare($sql);

	//Definição de parâmetros
	$resultados->bindParam(":nome_param", $pesq, PDO::PARAM_STR);
	$resultados->execute();ertertertertertert

	echo'<p>'.$sql.'</p><hr>';

	foreach($resultados as $linha)
	{
	echo '<p>';
	//Nome do campo na tabela pesquisada
	echo $linha["Nome_da_Coluna"];
	echo '</p>';
}

echo '<hr><p>Resultados: '.$resultados->rowCount().'</p>';

//Desconectar
$ligacao = null;
}*/
}
?>
