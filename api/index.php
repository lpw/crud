<?php
require 'vendor/autoload.php';




$router = new AltoRouter();
$router->setBasePath('crud/api/');
$router->map('GET|POST','/', 'home#index', 'home');

$router->map('GET','/tarefas/[i:id]',function($id){
	$tarefadao=new TarefaDAO();
	$tarefaController=new TarefaController($tarefadao);
	$tarefaController->Get($id);
	});

	$router->map('GET','/tarefas',function(){
		$tarefadao=new TarefaDAO();
		$tarefaController=new TarefaController($tarefadao);
		$tarefaController->ListAll();
});

	$router->map('PUT','/tarefas/[i:id]',function($id){
			$tarefadao=new TarefaDAO();
			$tarefaController=new TarefaController($tarefadao);
			$tarefaController->Put($id);
		});

$router->map('POST','/tarefas',function(){
		$tarefadao=new TarefaDAO();
		$tarefaController=new TarefaController($tarefadao);
     $tarefaController->Post();
});
$router->map('DELETE','/tarefas/[i:id]',function($id){
		$tarefadao=new TarefaDAO();
		$tarefaController=new TarefaController($tarefadao);
					$tarefaController->Delete($id);
		});

// match current request
$match = $router->match();


if( $match && is_callable( $match['target'] ) ) {
	call_user_func_array( $match['target'], $match['params'] );
} else {
	// no route was matched
	header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
}

?>
