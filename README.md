## Programa em PHP

### Pré requisitos

- Executar o script database.sql
- Alterar as configurações de banco de dados necessárias no arquivo crud/api/src/Conexao.php
- Liberar o mod_rewrite para o modo Allow All (para permiter que o .htaccess na pasta crud/api seja lida corretamente)


### Para executar

- Abrir o servidor em localhost/crud
